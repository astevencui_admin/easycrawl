FROM mcr.microsoft.com/playwright/java:v1.27.0-focal as builder

MAINTAINER dfsgg100@gmail.com

ENV TZ=Asia/Shanghai
ENV JAVA_OPTS="-Xms128m -Xmx256m -Djava.security.egd=file:/dev/./urandom"

RUN ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /lcshop-app

WORKDIR /lcshop-app

EXPOSE 8080

ADD ./target/easycrawl-web.jar ./

CMD java $JAVA_OPTS -jar easycrawl-web.jar --spring.profiles.active=docker
