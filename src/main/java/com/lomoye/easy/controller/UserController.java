package com.lomoye.easy.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import cn.hutool.http.HttpUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.lomoye.easy.model.User;
import com.lomoye.easy.model.tdm.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    final String secretKey = "22F2R7DX";
    final String DistributorId = "O00195";
    final String apiUrl = "http://ota.huanlepai.com.cn:8083/sc-ota-war/httpApi/xml";

    @GetMapping(value = "/json", produces = MediaType.APPLICATION_JSON_VALUE)
    public User index() {
        User user = new User("xiaoming", "18", "beijing");
        return user;
    }

    @GetMapping(value = "/xml", produces = MediaType.APPLICATION_XML_VALUE)
    public User XML(){
        User user = new User("xiaoming", "18", "beijing");
        return user;
    }

    @PostMapping(value = "/notify", produces = MediaType.APPLICATION_XML_VALUE)
    public OTMResponse OTTNotify() throws Exception {
        ScenicRequestBody requestBody = new ScenicRequestBody();
        XmlMapper xmlMapper = new XmlMapper();
        String body = xmlMapper.writeValueAsString(requestBody);
        OTMRequest request = new OTMRequest(body, "01");
        Map<String, Object> params = new HashMap<>();
        String xmlContent = xmlMapper.writeValueAsString(request);
        log.error(xmlContent);
        params.put("xmlContent", xmlContent);
        String result = HttpUtil.post(apiUrl,params);
        OTMResponse response = xmlMapper.readValue(result, OTMResponse.class);
        // 获取商品列表
        GoodsReq goodsReq = new GoodsReq();
        String goodsResBody = xmlMapper.writeValueAsString(goodsReq);
        log.error(goodsResBody);
        request = new OTMRequest(goodsResBody, "02");
        xmlContent = xmlMapper.writeValueAsString(request);
        log.error(xmlContent);
        params.put("xmlContent",xmlContent);
        result = HttpUtil.post(apiUrl,params);
        response = xmlMapper.readValue(result, OTMResponse.class);
        ResBody<GoodsRes> goodsRes = xmlMapper.readValue(response.getDecodeBody(), new TypeReference<ResBody<GoodsRes>>(){});


        return response;
    }
}
