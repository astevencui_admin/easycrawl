package com.lomoye.easy.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.base.Preconditions;
import com.lomoye.easy.domain.CommonDomain;
import com.lomoye.easy.domain.ConfigurableTxSpider;
import com.lomoye.easy.exception.BusinessException;
import com.lomoye.easy.model.ListPatternModel;
import com.lomoye.easy.model.NewsModel;
import com.lomoye.easy.model.SpiderAllModel;
import com.lomoye.easy.model.SpiderTestModel;
import com.lomoye.easy.model.common.ResultData;
import com.lomoye.easy.model.common.ResultList;
import com.lomoye.easy.model.common.ResultPagedList;
import com.lomoye.easy.model.search.ConfigurableSpiderSearchModel;
import com.lomoye.easy.model.vo.TxVo;
import com.lomoye.easy.service.ConfigurableTxSpiderService;
import com.lomoye.easy.service.DoWork;
import com.lomoye.easy.utils.HttpUtil;
import com.lomoye.easy.utils.MplusUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Arrays.asList;

@RestController
@RequestMapping({"/tx"})
@Api(
        tags = {"tx"},
        description = "tx抓取配置"
)
public class TxSpiderController {
    @Autowired
    private ConfigurableTxSpiderService configurableSpiderService;

    @PostMapping({"/search"})
    @ApiOperation("分页搜索")
    @ResponseBody
    public ResultPagedList<ConfigurableTxSpider> searchConfigurableSpider(@RequestBody ConfigurableSpiderSearchModel searchModel) {
        IPage<ConfigurableTxSpider> page = new Page(searchModel.getPageNo(), searchModel.getPageSize());
        LambdaQueryWrapper<ConfigurableTxSpider> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.
                like(!Strings.isNullOrEmpty(searchModel.getName()), ConfigurableTxSpider::getName, searchModel.getName()).
                like(!Strings.isNullOrEmpty(searchModel.getTableName()), ConfigurableTxSpider::getTableName, searchModel.getTableName()).
                orderByDesc(CommonDomain::getCreateTime);
        IPage<ConfigurableTxSpider> result = this.configurableSpiderService.page(page, queryWrapper);
        return new ResultPagedList(result.getRecords(), result.getTotal(), searchModel);
    }

    @PostMapping({"config"})
    @ApiOperation("增加一个tx爬虫配置")
    @ResponseBody
    public ResultData<ConfigurableTxSpider> addConfigurableSpider(@RequestBody ListPatternModel model) {
        if (model == null) {
            throw new BusinessException("val.parameter_illegal", "提交参数不能为空");
        } else if (Strings.isNullOrEmpty(model.getEntryUrl())) {
            throw new BusinessException("val.parameter_illegal", "入口页不能为空");
        } else if (Strings.isNullOrEmpty(model.getTableName())) {
            throw new BusinessException("val.parameter_illegal", "保存用的表名不能为空");
        } else if (!Strings.isNullOrEmpty(model.getFieldsJson()) && !"[]".equals(model.getFieldsJson())) {
            if (!Strings.isNullOrEmpty(model.getContentXpath()) && (Strings.isNullOrEmpty(model.getContentFieldsJson()) || "[]".equals(model.getContentFieldsJson()))) {
                throw new BusinessException("val.parameter_illegal", "正文页字段规则至少有一个");
            } else {
                QueryWrapper<ConfigurableTxSpider> queryWrapper = new QueryWrapper();
                queryWrapper.eq("table_name", model.getTableName());
                if (this.configurableSpiderService.count(queryWrapper) > 0) {
                    throw new BusinessException("val.parameter_illegal", "表名已被使用: " + model.getTableName());
                } else {
                    ConfigurableTxSpider spider = ConfigurableTxSpider.valueOf(model);
                    this.configurableSpiderService.save(spider);
                    return new ResultData(spider);
                }
            }
        } else {
            throw new BusinessException("val.parameter_illegal", "入口页字段规则至少有一个");
        }
    }

    @PutMapping
    @ApiOperation("跟新一个可配置化蜘蛛")
    @ResponseBody
    public ResultData<ConfigurableTxSpider> updateConfigurableSpider(@RequestBody ConfigurableTxSpider model) {
        if (model == null) {
            throw new BusinessException("val.parameter_illegal", "提交参数不能为空");
        } else if (Strings.isNullOrEmpty(model.getEntryUrl())) {
            throw new BusinessException("val.parameter_illegal", "入口页不能为空");
        } else if (Strings.isNullOrEmpty(model.getTableName())) {
            throw new BusinessException("val.parameter_illegal", "保存用的表名不能为空");
        } else if (!Strings.isNullOrEmpty(model.getFieldsJson()) && !"[]".equals(model.getFieldsJson())) {
            if (model.getId() == null) {
                throw new BusinessException("val.parameter_illegal", "蜘蛛id不能为空");
            } else {
                ConfigurableTxSpider spider = (ConfigurableTxSpider)this.configurableSpiderService.getById(model.getId());
                if (spider == null) {
                    throw new BusinessException("val.parameter_illegal", "蜘蛛不存在");
                } else if (!model.getTableName().equals(spider.getTableName())) {
                    throw new BusinessException("val.parameter_illegal", "表名不支持修改");
                } else {
                    model.setModifyTime(LocalDateTime.now());
                    this.configurableSpiderService.updateById(model);
                    return new ResultData(spider);
                }
            }
        } else {
            throw new BusinessException("val.parameter_illegal", "入口页字段规则至少有一个");
        }
    }

    @PostMapping({"/startNews"})
    @ApiOperation("抓取news")
    @ResponseBody
    public ResultList<String> startNews(@RequestBody SpiderTestModel testModel) {
        if (Strings.isNullOrEmpty(testModel.getUrl())) {
            throw new BusinessException("val.parameter_illegal", "请输入网址");
        } else if (Strings.isNullOrEmpty(testModel.getXpath())) {
            throw new BusinessException("val.parameter_illegal", "请输入XPATH");
        } else {
            Html html = this.downloadPage(testModel.getUrl(), testModel.getIsDynamic());
            return new ResultList(html.xpath(testModel.getXpath()).all());
        }
    }

    @PostMapping({"/test-xpath"})
    @ApiOperation("测试xpath")
    @ResponseBody
    public ResultData<Object> testXpath(@RequestBody SpiderTestModel testModel) throws MalformedURLException {

        Map<String, Object> data = spOne(testModel);
        return new ResultData(data);
    }

    @PostMapping({"/spider-all"})
    @ApiOperation("测试xpath")
    @ResponseBody
    public ResultData<Object> spiderAll(@RequestBody SpiderAllModel allModel) throws MalformedURLException, InterruptedException, ExecutionException {
        String regex = "(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]";


        List<DoWork> doWorkList = filterSpecialStr(regex, allModel.getUrls());


        ExecutorService executor = Executors.newFixedThreadPool(2);
        List<Future<TxVo>> results = executor.invokeAll(doWorkList);
        executor.shutdown();

        List<TxVo> voList = new ArrayList<>();
        for (int i = 0; i < results.size(); i++) {
            voList.add(results.get(i).get());
        }

        return new ResultData(voList);
    }


    private Map<String, Object> spOne(SpiderTestModel testModel) throws MalformedURLException {
        String url = testModel.getUrl();
        String urlPath = url.substring(0, url.lastIndexOf("/") + 1);
        URL uri = new URL(url);
        String firstPath = uri.getProtocol() + "://" + uri.getHost() + "/";
        LambdaQueryWrapper<ConfigurableTxSpider> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(ConfigurableTxSpider::getEntryUrl, uri.getHost());
        Map<String, Object> data = new HashMap();
        List<ConfigurableTxSpider> txSpiderList = this.configurableSpiderService.list(queryWrapper);
        for (int index = 0; index < txSpiderList.size(); index++) {
            ConfigurableTxSpider txSpider = txSpiderList.get(index);
            txSpider.setFields(this.configurableSpiderService.parseFields(txSpider.getFieldsJson()));
            txSpider.setContentFields(this.configurableSpiderService.parseFields(txSpider.getContentFieldsJson()));
            Html html = this.downloadPage(testModel.getUrl(), txSpider.getIsDynamic());
            List<String> titles = html.xpath((String)txSpider.getFields().get("title")).all();

            Selectable content = html.xpath((String)txSpider.getContentFields().get("content"));
            List<String> imgSrcs = content.xpath("//img/@src").all();
            List<String> contentList = content.all();
            if (contentList.size() == 0 || titles.size() == 0){
                continue;
            }
            String strContent = contentList.get(0);

            for(int i = 0; i < imgSrcs.size(); ++i) {
                String imgSrc = (String)imgSrcs.get(i);
                if (StringUtils.isEmpty(imgSrc))continue;
                if (imgSrc.startsWith("//")) {
                    strContent = strContent.replace(imgSrc, uri.getProtocol() + ":" + (String)imgSrcs.get(i));
                } else if (imgSrc.startsWith("/")) {
                    strContent = strContent.replace(imgSrc, firstPath + (String)imgSrcs.get(i));
                } else {
                    if (imgSrc.startsWith("http")) {
                        break;
                    }

                    strContent = strContent.replace((CharSequence)imgSrcs.get(i), urlPath + (String)imgSrcs.get(i));
                }
            }
            String title =  titles.get(0);
            data.put("title", title);
            data.put("content", strContent);
            break;
        }
        return data;
    }

    @PostMapping({"/news"})
    @ApiOperation("提交新闻到plus后台")
    @ResponseBody
    public ResultData postToMob(@RequestBody NewsModel model) {
        String result = MplusUtil.insertNews(model.getTitle(), model.getContent());
        JSON object = JSON.parseObject(result);
        return new ResultData(object);
    }

    @DeleteMapping({"/{id}"})
    @ApiOperation("删除爬虫")
    @ResponseBody
    public ResultData<Boolean> deleteConfigurableSpider(@PathVariable String id) {
        Preconditions.checkArgument(id != null);
        this.configurableSpiderService.removeById(id);
        return new ResultData(true);
    }

    private Html downloadPage(String url, Integer isDynamic) {
        Html html;
        if (isDynamic != null && isDynamic == 1) {
            html = HttpUtil.downloadDynamic(url);
        } else {
            html = HttpUtil.download(url);
        }

        if (html == null) {
            throw new BusinessException("val.parameter_illegal", "网页下载失败");
        } else {
            return html;
        }
    }

    /**
     * 参数1 regex:我们的正则字符串
     * 参数2 就是一大段文本，这里用data表示
     */
    private List<DoWork> filterSpecialStr(String regex, String data) {
        //sb存放正则匹配的结果
        List<DoWork> sb = new ArrayList<>();
        //编译正则字符串
        Pattern p = Pattern.compile(regex);
        //利用正则去匹配
        Matcher matcher = p.matcher(data);
        //如果找到了我们正则里要的东西
        while (matcher.find()) {
            //保存到sb中，"\r\n"表示找到一个放一行，就是换行
            SpiderTestModel model = new SpiderTestModel();
            model.setUrl(matcher.group());
            DoWork doWork = new DoWork(model, configurableSpiderService);
            sb.add(doWork);
        }
        return sb;
    }


}
