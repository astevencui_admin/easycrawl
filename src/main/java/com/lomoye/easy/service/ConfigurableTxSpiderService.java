package com.lomoye.easy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lomoye.easy.domain.ConfigurableTxSpider;
import com.lomoye.easy.model.SpiderTestModel;
import com.lomoye.easy.model.vo.TxVo;

import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface ConfigurableTxSpiderService extends IService<ConfigurableTxSpider> {
    void saveData(List<LinkedHashMap<String, String>> datas, ConfigurableTxSpider spider, String uuid);

    LinkedHashMap<String, String> parseFields(String fieldsJson);

    TxVo spOne(SpiderTestModel testModel) throws MalformedURLException;
}
