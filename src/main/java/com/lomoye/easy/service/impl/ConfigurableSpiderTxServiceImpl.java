package com.lomoye.easy.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lomoye.easy.dao.ConfigurableTxSpiderMapper;
import com.lomoye.easy.domain.ConfigurableTxSpider;
import com.lomoye.easy.exception.BusinessException;
import com.lomoye.easy.model.SpiderTestModel;
import com.lomoye.easy.model.vo.TxVo;
import com.lomoye.easy.service.ConfigurableTxSpiderService;
import com.lomoye.easy.utils.HttpUtil;
import com.lomoye.easy.utils.LocalDateUtil;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.assertj.core.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
public class ConfigurableSpiderTxServiceImpl extends ServiceImpl<ConfigurableTxSpiderMapper, ConfigurableTxSpider> implements ConfigurableTxSpiderService {
    private static final String COMMON_FIELD = "job_uuid";
    @Autowired
    private HikariDataSource dataSource;

    public ConfigurableSpiderTxServiceImpl() {
    }

    public void saveData(List<LinkedHashMap<String, String>> datas, ConfigurableTxSpider spider, String uuid) {
        log.info("start saveData");
        if (datas != null && !datas.isEmpty()) {
            try {
                this.doSaveData(datas, spider, uuid);
            } catch (Exception var5) {
                log.error("saveData error", var5);
            }

        } else {
            log.info("datas empty");
        }
    }

    public LinkedHashMap<String, String> parseFields(String fieldsJson) {
        if (Strings.isNullOrEmpty(fieldsJson)) {
            return new LinkedHashMap();
        } else {
            LinkedHashMap<String, String> map = new LinkedHashMap();
            JSONArray fields = JSON.parseArray(fieldsJson);
            Iterator var4 = fields.iterator();

            while(var4.hasNext()) {
                Object fieldObj = var4.next();
                JSONObject field = (JSONObject)fieldObj;
                map.put(field.get("key").toString(), field.get("value").toString());
            }

            return map;
        }
    }

    private void doSaveData(List<LinkedHashMap<String, String>> datas, ConfigurableTxSpider metaInfo, String uuid) throws SQLException, ClassNotFoundException {
        LinkedHashMap<String, String> fields = new LinkedHashMap();
        fields.putAll(metaInfo.getFields());
        if (metaInfo.getContentFields() != null) {
            fields.putAll(metaInfo.getContentFields());
        }

        String sql = this.getSql(datas, metaInfo, fields, uuid);
        log.info("doSaveData sql={}", sql);
        Connection connection = this.getConnection();
        Statement statement = connection.createStatement();

        try {
            statement.execute(sql);
        } catch (Exception var15) {
            log.error("doSaveData error", var15);

            try {
                this.createTable(metaInfo.getTableName(), statement, fields);
            } catch (Exception var14) {
                log.error("doSaveData createTable error", var14);
            }

            statement.execute(sql);
        } finally {
            statement.close();
            connection.close();
        }

    }

    private void createTable(String tableName, Statement statement, LinkedHashMap<String, String> fields) throws SQLException {
        StringBuilder sql = new StringBuilder("CREATE TABLE IF NOT EXISTS " + tableName + "\n");
        StringBuilder fieldDefs = new StringBuilder("( ");
        fieldDefs.append("`").append("id").append("`").append(" BIGINT(20) NOT NULL AUTO_INCREMENT ").append(",\n");
        fields.forEach((k, v) -> {
            StringBuilder fieldDef = new StringBuilder();
            fieldDef.append("`").append(k).append("`").append(" text ").append(",\n");
            fieldDefs.append(fieldDef);
        });
        fieldDefs.append("`").append("job_uuid").append("`").append(" CHAR(64) ").append(",\n");
        fieldDefs.append("`").append("create_time").append("`").append(" DATETIME ").append(",\n");
        fieldDefs.append("`").append("modify_time").append("`").append(" DATETIME ").append(",\n");
        fieldDefs.append("PRIMARY KEY (id) \n");
        fieldDefs.append(")");
        sql.append(fieldDefs);
        log.info("createTable sql={}", sql);
        statement.executeUpdate(sql.toString());
    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        return this.dataSource.getConnection();
    }

    private String getSql(List<LinkedHashMap<String, String>> datas, ConfigurableTxSpider metaInfo, LinkedHashMap<String, String> fields, String uuid) {
        StringBuilder fieldsStr = new StringBuilder();
        fields.forEach((k, v) -> {
            fieldsStr.append("`").append(k).append("`").append(",");
        });
        fieldsStr.append("`").append("job_uuid").append("`");
        fieldsStr.append(",`").append("create_time").append("`");
        fieldsStr.append(",`").append("modify_time").append("`");
        StringBuilder sql = new StringBuilder("INSERT INTO `" + metaInfo.getTableName() + "` (" + fieldsStr + ") VALUES ");
        StringBuilder values = new StringBuilder();
        datas.forEach((data) -> {
            StringBuilder sqlValue = new StringBuilder();
            data.forEach((k, v) -> {
                sqlValue.append("'").append(StringEscapeUtils.escapeSql(v)).append("'").append(",");
            });
            sqlValue.append("'").append(uuid).append("'");
            sqlValue.append(",'").append(LocalDateUtil.getDateTimeAsString(LocalDateTime.now())).append("'");
            sqlValue.append(",'").append(LocalDateUtil.getDateTimeAsString(LocalDateTime.now())).append("'");
            values.append(",(").append(sqlValue).append(" )");
        });
        sql.append(values.substring(1, values.length()));
        return sql.toString();
    }

    @Override
    public TxVo spOne(SpiderTestModel testModel) throws MalformedURLException {
        String url = testModel.getUrl();
        String urlPath = url.substring(0, url.lastIndexOf("/") + 1);
        URL uri = new URL(url);
        String firstPath = uri.getProtocol() + "://" + uri.getHost() + "/";
        LambdaQueryWrapper<ConfigurableTxSpider> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(ConfigurableTxSpider::getEntryUrl, uri.getHost());
        TxVo data = new TxVo();
        List<ConfigurableTxSpider> txSpiderList = this.list(queryWrapper);
        for (int index = 0; index < txSpiderList.size(); index++) {
            ConfigurableTxSpider txSpider = txSpiderList.get(index);
            txSpider.setFields(this.parseFields(txSpider.getFieldsJson()));
            txSpider.setContentFields(this.parseFields(txSpider.getContentFieldsJson()));
            Html html = this.downloadPage(testModel.getUrl(), txSpider.getIsDynamic());
            List<String> titles = html.xpath((String)txSpider.getFields().get("title")).all();

            Selectable content = html.xpath((String)txSpider.getContentFields().get("content"));
            List<String> imgSrcs = content.xpath("//img/@src").all();
            List<String> contentList = content.all();
            if (contentList.size() == 0 || titles.size() == 0){
                continue;
            }
            String strContent = contentList.get(0);

            for(int i = 0; i < imgSrcs.size(); ++i) {
                String imgSrc = (String)imgSrcs.get(i);
                if (StringUtils.isEmpty(imgSrc))continue;
                if (imgSrc.startsWith("//")) {
                    strContent = strContent.replace(imgSrc, uri.getProtocol() + ":" + (String)imgSrcs.get(i));
                } else if (imgSrc.startsWith("/")) {
                    strContent = strContent.replace(imgSrc, firstPath + (String)imgSrcs.get(i));
                } else {
                    if (imgSrc.startsWith("http")) {
                        break;
                    }

                    strContent = strContent.replace((CharSequence)imgSrcs.get(i), urlPath + (String)imgSrcs.get(i));
                }
            }
            String title =  titles.get(0);
            data.setUrl(url);
            data.setTitle(title);
            data.setContent(strContent);
            break;
        }
        return data;
    }

    private Html downloadPage(String url, Integer isDynamic) {
        Html html;
        if (isDynamic != null && isDynamic == 1) {
            html = HttpUtil.downloadDynamic(url);
        } else {
            html = HttpUtil.download(url);
        }

        if (html == null) {
            throw new BusinessException("val.parameter_illegal", "网页下载失败");
        } else {
            return html;
        }
    }

}

