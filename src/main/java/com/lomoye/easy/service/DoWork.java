package com.lomoye.easy.service;


import com.lomoye.easy.model.SpiderTestModel;
import com.lomoye.easy.model.vo.TxVo;

import java.util.List;
import java.util.concurrent.Callable;

public class DoWork implements Callable<TxVo> {

    /**
     * 需要处理的对象集合，每个线程传递自己的对象.
     */
    SpiderTestModel model;

    ConfigurableTxSpiderService spiderService;

    public DoWork(SpiderTestModel model, ConfigurableTxSpiderService service) {
        this.model = model;
        this.spiderService = service;
    }

    @Override
    public TxVo call() throws Exception {
        TxVo vo  = spiderService.spOne(model);
        return vo;
    }
}
