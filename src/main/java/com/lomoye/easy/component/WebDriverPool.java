package com.lomoye.easy.component;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author code4crafter@gmail.com <br>
 * Date: 13-7-26 <br>
 * Time: 下午1:41 <br>
 */
@Slf4j
class WebDriverPool {

    private final static int DEFAULT_CAPACITY = 5;

    private final int capacity;

    private final static int STAT_RUNNING = 1;

    private final static int STAT_CLODED = 2;

    private AtomicInteger stat = new AtomicInteger(STAT_RUNNING);

    protected static DesiredCapabilities sCaps;

    Playwright playwright = Playwright.create();


    public Browser newDriver() throws IOException {
        // Prepare capabilities
        BrowserType.LaunchOptions options =  new BrowserType.LaunchOptions();
        List<String> args = new ArrayList<>();
        //设置 chrome 的无头模式
        args.add("--headless");
        args.add("--disable-gpu");
        args.add("--no-sandbox");
        args.add("--disable-dev-shm-usage");
        args.add("--start-maximized");
        //因为报表页面必须滚动才能全部展示，这里直接给个很大的高度
        args.add("--window-size=1280,4300");
        options.setArgs(args);

        Browser browser = playwright.chromium().launch(options.setHeadless(true));

        return browser;
    }

    /**
     * store webDrivers created
     */
    private final List<Browser> webDriverList = Collections
            .synchronizedList(new ArrayList<>());

    /**
     * store webDrivers available
     */
    private final BlockingDeque<Browser> innerQueue = new LinkedBlockingDeque<Browser>();

    public WebDriverPool(int capacity) {
        this.capacity = capacity;
    }

    public WebDriverPool() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * @return
     * @throws InterruptedException
     */
    public Browser get() throws InterruptedException {
        checkRunning();
        Browser poll = innerQueue.poll();
        if (poll != null) {
            return poll;
        }
        if (webDriverList.size() < capacity) {
            synchronized (webDriverList) {
                if (webDriverList.size() < capacity) {

                    // add new WebDriver instance into pool
                    try {
                        Browser mDriver = newDriver();
                        innerQueue.add(mDriver);
                        webDriverList.add(mDriver);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        return innerQueue.take();
    }

    public void returnToPool(Browser webDriver) {
        checkRunning();
        innerQueue.add(webDriver);
    }

    protected void checkRunning() {
        if (!stat.compareAndSet(STAT_RUNNING, STAT_RUNNING)) {
            throw new IllegalStateException("Already closed!");
        }
    }

    public void closeAll() {
        log.info("start closeAll web driver");
        boolean b = stat.compareAndSet(STAT_RUNNING, STAT_CLODED);
        if (!b) {
            throw new IllegalStateException("Already closed!");
        }
        for (Browser webDriver : webDriverList) {
            log.info("Quit webDriver" + webDriver);
            webDriver.close();
        }
        playwright.close();
    }

}
