package com.lomoye.easy.component;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.LoadState;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.downloader.AbstractDownloader;
import us.codecraft.webmagic.downloader.Downloader;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.PlainText;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;

/**
 * 使用Selenium调用浏览器进行渲染。目前仅支持chrome。<br>
 * 需要下载Selenium driver支持。<br>
 *
 * @author code4crafter@gmail.com <br>
 *         Date: 13-7-26 <br>
 *         Time: 下午1:37 <br>
 */
public class SeleniumDownloader extends AbstractDownloader implements Downloader {

	private volatile WebDriverPool webDriverPool;

	private Logger logger = Logger.getLogger(getClass());

	private int sleepTime = 0;

	private int poolSize = 1;


	/**
	 * 新建
	 *
	 * @param chromeDriverPath chromeDriverPath
	 */
	public SeleniumDownloader(String chromeDriverPath) {
		System.getProperties().setProperty("webdriver.chrome.driver",
				chromeDriverPath);
	}

	/**
	 * set sleep time to wait until load success
	 *
	 * @param sleepTime sleepTime
	 * @return this
	 */
	public SeleniumDownloader setSleepTime(int sleepTime) {
		this.sleepTime = sleepTime;
		return this;
	}

	@Override
	public Page download(Request request, Task task) {
		checkInit();
		Browser webDriver;
		try {
			webDriver = webDriverPool.get();
		} catch (InterruptedException e) {
			logger.warn("interrupted", e);
			return null;
		}
		logger.info("downloading page " + request.getUrl());
		com.microsoft.playwright.Page page = webDriver.newPage();
		page.navigate(request.getUrl());
		page.waitForLoadState(LoadState.NETWORKIDLE);
		String html = page.content();
		page.close();
		/*
		 * TODO You can add mouse event or other processes
		 */

		Page result = new Page();
		result.setRawText(html);
		result.setHtml(new Html(html, request.getUrl()));
		result.setUrl(new PlainText(request.getUrl()));
		result.setRequest(request);
		webDriverPool.returnToPool(webDriver);
		return result;
	}

//	@Override
//	public Page download(Request request, Task task) {
//		try {
//			Playwright playwright = Playwright.create();
//
//			Page var8;
//			try {
//				Browser browser = playwright.webkit().launch((new BrowserType.LaunchOptions()).setHeadless(true));
//				com.microsoft.playwright.Page page = browser.newPage();
//				page.navigate(request.getUrl());
//				page.waitForLoadState(LoadState.NETWORKIDLE);
//				String html = page.content();
//				page.close();
//				Page pageRes = new Page();
//				pageRes.setRawText(html);
//				pageRes.setHtml(new Html(html, request.getUrl()));
//				pageRes.setUrl(new PlainText(request.getUrl()));
//				pageRes.setRequest(request);
//				var8 = pageRes;
//			} catch (Throwable var10) {
//				if (playwright != null) {
//					try {
//						playwright.close();
//					} catch (Throwable var9) {
//						var10.addSuppressed(var9);
//					}
//				}
//
//				throw var10;
//			}
//
//			if (playwright != null) {
//				playwright.close();
//			}
//
//			return var8;
//		} catch (Exception var11) {
//			this.logger.error("interrupted", var11);
//			return null;
//		}
//	}

	private void checkInit() {
		if (webDriverPool == null) {
			synchronized (this) {
				webDriverPool = new WebDriverPool(poolSize);
			}
		}
	}

	@Override
	public void setThread(int thread) {
		this.poolSize = thread;
	}


	public void close() throws IOException {
		webDriverPool.closeAll();
	}
}
