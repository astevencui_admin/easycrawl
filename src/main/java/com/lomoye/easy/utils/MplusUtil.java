package com.lomoye.easy.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpUtil;
import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import com.microsoft.playwright.options.LoadState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class MplusUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(MplusUtil.class);

    public MplusUtil() {
    }

    public static void uploadImgByUrls(String url) {
    }

    public static String insertNews(String title, String content) {
        String index_pic = "http://img1.habctv.com/material/news/img/640x/2020/11/20201127120217z2bU.png?Dh7D";
        long publish_time = DateUtil.currentSeconds();
        String access_key = "coBEJT7S5qQSyHTWwM26FdfkEQcF3lk2";
        String access_secret = "Wbd5stbDnLJEAUWnb987ewTyt7ytdXEs";
        String site_id = "1";
        String category_id = "83";
        String publish_column = "278";
        Map<String, Object> map = new HashMap();
        map.put("title", title);
        map.put("category_id", category_id);
        map.put("content", content);
        map.put("access_key", access_key);
        map.put("access_secret", access_secret);
        map.put("publish_column", publish_column);
        map.put("author", "");
        map.put("source", "");
        map.put("site_id", site_id);
        map.put("publish_time", publish_time);
        map.put("index_pic", index_pic);
        String apiurl = "http://m2o-api.huaihai.tv/api/internal/news";
        String result = HttpUtil.createPost(apiurl).form(map).execute().body();
        LOGGER.debug(result);
        return result;
    }

    public static String test() {
        Playwright playwright = Playwright.create();

        String var4;
        try {
            Browser browser = playwright.chromium().launch();
            Page page = browser.newPage();
            page.navigate("http://www.chinanews.com.cn/gn/2022/09-23/9858770.shtml");
            page.waitForLoadState(LoadState.NETWORKIDLE);
            String html = page.content();
            page.close();
            var4 = html;
        } catch (Throwable var6) {
            if (playwright != null) {
                try {
                    playwright.close();
                } catch (Throwable var5) {
                    var6.addSuppressed(var5);
                }
            }

            throw var6;
        }

        if (playwright != null) {
            playwright.close();
        }

        return var4;
    }
}
