package com.lomoye.easy.model.vo;

import lombok.Data;

@Data
public class TxVo {
    private String url;
    private String title;
    private String content;
}
