package com.lomoye.easy.model.tdm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "Body")
public class GoodsReq {
    @JacksonXmlProperty(localName = "ScenicId")
    String ScenicId = "S0008";
    @JacksonXmlProperty(localName = "GoodsId")
    String GoodsId;
    @JacksonXmlProperty(localName = "PageIndex")
    Long pageIndex;
    @JacksonXmlProperty(localName = "PageSize")
    Long pageSize;
}
