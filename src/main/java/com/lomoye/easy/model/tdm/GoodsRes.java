package com.lomoye.easy.model.tdm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "Goods")
public class GoodsRes {
    // 景点id
    @JacksonXmlProperty(localName = "ScenicId")
    String      ScenicId;
    // 景点名称
    @JacksonXmlProperty(localName = "GoodsId")
    String      GoodsId;
    // 商品名
    @JacksonXmlProperty(localName = "GoodsName")
    String      GoodsName;
    @JacksonXmlProperty(localName = "Cover")
    String Cover;
    // 剩余可购买数量
    @JacksonXmlProperty(localName = "LimitCount")
    String LimitCount;
    // 每天可购买的最大数量
    @JacksonXmlProperty(localName = "DayMaxSellNum")
    String DayMaxSellNum;
    // 商品可购买开始时间
    @JacksonXmlProperty(localName = "ValidityBuyStart")
    String ValidityBuyStart;
    // 商品可购买结束时间
    @JacksonXmlProperty(localName = "ValidityBuyEnd")
    String ValidityBuyEnd;
    // 当天最后可购买时间
    @JacksonXmlProperty(localName = "CurrentDayLastBuyTime")
    String CurrentDayLastBuyTime;
    // 可提前下单的最大天数
    @JacksonXmlProperty(localName = "MaxAdvanceDays")
    String MaxAdvanceDays;
    // 单笔订单最大商品份数
    @JacksonXmlProperty(localName = "MaxQuantityEachOrder")
    String MaxQuantityEachOrder;
    // 凭证打印模式
    @JacksonXmlProperty(localName = "PrintModel")
    String PrintModel;
    // 允许支付开始时间
    @JacksonXmlProperty(localName = "CanPayBeginTime")
    String CanPayBeginTime;
    // 允许支付结束时间
    @JacksonXmlProperty(localName = "CanPayEndTime")
    String CanPayEndTime;
    // 商品说明
    @JacksonXmlProperty(localName = "Description")
    String Description;
    // 游客提示
    @JacksonXmlProperty(localName = "GuestPrompt")
    String GuestPrompt;
    // 门市价
    @JacksonXmlProperty(localName = "SalesPrice")
    String SalesPrice;
    // 支付方式
    @JacksonXmlProperty(localName = "PayType")
    String PayType;
    // 指导销售价范围
    @JacksonXmlProperty(localName = "GuidingPriceRange")
    String GuidingPriceRange;
    // 证件类型
    @JacksonXmlProperty(localName = "CertificateType")
    String CertificateType;
    // 商品有效期类型
    @JacksonXmlProperty(localName = "ValidityType")
    String ValidityType;

    @JacksonXmlProperty(localName = "GoodsTimeInfos")
    List<GoodsTimeInfo> GoodsTimeInfos;
}
