package com.lomoye.easy.model.tdm;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "Body")
public class PreOrderReq {
    // OTA订单编号
    @JacksonXmlProperty(localName = "OutOrderId")
    String OutOrderId;
    // 客户姓名
    @JacksonXmlProperty(localName = "Name")
    String Name;
    // 客户电话
    @JacksonXmlProperty(localName = "Mobile")
    String Mobile;
    // 商品数量
    @JacksonXmlProperty(localName = "Quantity")
    String Quantity;
    // 商品编号
    @JacksonXmlProperty(localName = "GoodsId")
    String GoodsId;
    // 销售单价
    @JacksonXmlProperty(localName = "SalePrice")
    String SalePrice;
    //短信发送模式
    @JacksonXmlProperty(localName = "SmsSendMode")
    String SmsSendMode;
    // 证件类型
    @JacksonXmlProperty(localName = "CertificateType")
    String CertificateType;
    //证件号
    @JacksonXmlProperty(localName = "CertificateNum")
    String CertificateNum;
    // 指定出行日期
    @JacksonXmlProperty(localName = "AppointTripDate")
    String AppointTripDate;

}
