package com.lomoye.easy.model.tdm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@Data
public class JDBody {
    @JacksonXmlProperty(localName = "ScenicId")
    String ScenicId;
    @JacksonXmlProperty(localName = "ScenicName")
    String ScenicName;
}
