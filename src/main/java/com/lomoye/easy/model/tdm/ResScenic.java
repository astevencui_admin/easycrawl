package com.lomoye.easy.model.tdm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "Scenic")
public class ResScenic {
    // 景点id
    @JacksonXmlProperty(localName = "ScenicId")
    String      ScenicId;
    // 景点名称
    @JacksonXmlProperty(localName = "ScenicName")
    String      ScenicName;
    // 景点详情
    @JacksonXmlProperty(localName = "ScenicDetail")
    String      ScenicDetail;
}
