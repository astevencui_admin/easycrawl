package com.lomoye.easy.model.tdm;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "OTM")
public class OTMResponse {
    @JacksonXmlProperty(localName = "Head")
    ResHead head;
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Body")
    String body;


    String decodeBody;


//    /**
//     * 获取 T.class
//     */
//    public Class<T> getTClass() {
//        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
//    }

//
//    public void setBody(String body) throws UnsupportedEncodingException {
//        this.body = body;
//        String decodeStr = Base64.decodeStr(body, CharsetUtil.CHARSET_UTF_8);
//        SymmetricCrypto des = new SymmetricCrypto(SymmetricAlgorithm.DES, "22F2R7DX".getBytes("UTF-8"));
//        String decodeBody = des.decryptStr(decodeStr);
//        XmlMapper xmlMapper = new XmlMapper();
//        try {
//            resBody = xmlMapper.readValue(decodeBody, ResBody.class);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public void setBody(String body) throws UnsupportedEncodingException {
        this.body = body;
        String decodeStr = Base64.decodeStr(body, CharsetUtil.CHARSET_UTF_8);
        SymmetricCrypto des = new SymmetricCrypto(SymmetricAlgorithm.DES, "22F2R7DX".getBytes("UTF-8"));
        decodeBody = des.decryptStr(decodeStr);
//        XmlMapper xmlMapper = new XmlMapper();
//        try {
//            resBody = xmlMapper.readValue(decodeBody, ResBody.class);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
    }
}

