package com.lomoye.easy.model.tdm;


import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

@JacksonXmlRootElement(localName = "Body")
@Data
public class ScenicRequestBody {
    @JacksonXmlProperty(localName = "ScenicId")
    String ScenicId = "S0008";
    @JacksonXmlProperty(localName = "ScenicName")
    String ScenicName = "";
}
