package com.lomoye.easy.model.tdm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "Body")
public class ResBody<T> {
    @JacksonXmlProperty(localName = "Count")
    Long count;
    @JacksonXmlProperty(localName = "PageIndex")
    Long pageIndex;
    @JacksonXmlProperty(localName = "PageSize")
    Long pageSize;
    @JacksonXmlProperty(localName = "Items")
    List<T> items;
}
