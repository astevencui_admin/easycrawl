package com.lomoye.easy.model.tdm;

public class Voucher {
    // 是否是套票
    Long IsPackage;
    // 凭证ID
    String VoucherId;
    // 凭证值
    String VoucherValue;
    // 凭证图片地址
    String VoucherImageUrl;
    // 凭证最大使用数量
    Long VoucherMaxUseTimes;
    // 凭证开始使用时间
    String StartUseDate;
    // 凭证截至使用时间;
    String EndUseDate;
    // 凭证每天开始使用时间
    String StartExpAllowTime;
    // 凭证每天结束使用时间;
    String EndExpAllowTime;
}
