package com.lomoye.easy.model.tdm;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

@Data
public class Head {
    @JacksonXmlProperty(localName = "Version")
    String Version = "2.1.0";
    @JacksonXmlProperty(localName = "SequenceId")
    String SequenceId;
    @JacksonXmlProperty(localName = "TimeStamp")
    String TimeStamp;
    @JacksonXmlProperty(localName = "DistributorId")
    String DistributorId = "O00195";
    @JacksonXmlProperty(localName = "TransactionCode")
    String TransactionCode;
    // 1）消息签名规则：Base64(MD5(消息序号+DistributorId+消息体内容长度))。
    @JacksonXmlProperty(localName = "Signed")
    String Signed;
    public Head(String body, String TransactionCode){
        SequenceId = "" + DateUtil.currentSeconds();
        TimeStamp = DateUtil.now();
        this.TransactionCode = TransactionCode;
        String md5Str = DigestUtil.md5Hex(SequenceId+DistributorId + body.length());
        this.Signed = Base64.encode(md5Str);
    }
}
