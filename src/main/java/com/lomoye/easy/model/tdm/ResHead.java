package com.lomoye.easy.model.tdm;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResHead {
    @JacksonXmlProperty(localName = "Version")
    String Version = "2.1.0";
    @JacksonXmlProperty(localName = "SequenceId")
    String SequenceId;
    @JacksonXmlProperty(localName = "TimeStamp")
    String TimeStamp;
    @JacksonXmlProperty(localName = "DistributorId")
    String DistributorId = "O00195";
    @JacksonXmlProperty(localName = "TransactionCode")
    String TransactionCode;
    @JacksonXmlProperty(localName = "StatusCode")
    String StatusCode;
    @JacksonXmlProperty(localName = "Message")
    String Message;
    // 1）消息签名规则：Base64(MD5(消息序号+DistributorId+消息体内容长度))。
    @JacksonXmlProperty(localName = "Signed")
    String Signed;
    public ResHead(String body, String TransactionCode){
        SequenceId = "" + DateUtil.currentSeconds();
        TimeStamp = DateUtil.now();
        this.TransactionCode = TransactionCode;
        String md5Str = DigestUtil.md5Hex(SequenceId+DistributorId + body.length());
        this.Signed = Base64.encode(md5Str);
    }
}
