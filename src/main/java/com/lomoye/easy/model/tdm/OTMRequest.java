package com.lomoye.easy.model.tdm;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "OTM")
public class OTMRequest {
    @JacksonXmlProperty(localName = "Head")
    Head Head;
    //  @JacksonXmlCData 解析 <![CDATA[ ]]>
    @JacksonXmlCData
    // @JacksonXmlProperty xml 名称
    @JacksonXmlProperty(localName = "Body")
    String Body;

    public OTMRequest(String body, String TransactionCode){
        this.Head = new Head(body, TransactionCode);
        SymmetricCrypto des = new SymmetricCrypto(SymmetricAlgorithm.DES, "22F2R7DX".getBytes());
        String encodeBody = Base64.encode(des.encryptHex(body));
        this.Body = encodeBody;

    }
}
