package com.lomoye.easy.model.tdm;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JacksonXmlRootElement(localName = "GoodsTimeInfo")
public class GoodsTimeInfo {
    // 商品有效期开始时间
    @JacksonXmlProperty(localName = "ValidityStart")
    String ValidityStart;
    // 商品有效期结束时间
    @JacksonXmlProperty(localName = "ValidityEnd")
    String ValidityEnd;
    //下单后延迟生效时间
    @JacksonXmlProperty(localName = "DelayEffectTime")
    String DelayEffectTime;
    // 商品生效后持续有效时间
    @JacksonXmlProperty(localName = "EffectEndTime")
    String EffectEndTime;
    // 可指定出行日的备选日
    @JacksonXmlProperty(localName = "OptionalDay")
    String OptionalDay;
    // 有效周次
    @JacksonXmlProperty(localName = "ValidityWeekDay")
    String ValidityWeekDay;
    // 特殊日内不能使用;
    @JacksonXmlProperty(localName = "ValiditySpecialDay")
    String ValiditySpecialDay;
    // 每天有效开始时间;
    @JacksonXmlProperty(localName = "StartExpAllowTime")
    String StartExpAllowTime;
    // 每天有效结束时间
    @JacksonXmlProperty(localName = "EndExpAllowTime")
    String EndExpAllowTime;
    // OTA结算价
    @JacksonXmlProperty(localName = "SettlementPrice")
    String SettlementPrice;
}
