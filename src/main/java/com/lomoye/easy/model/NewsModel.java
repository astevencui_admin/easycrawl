package com.lomoye.easy.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class NewsModel {
    @ApiModelProperty(
            name = "标题"
    )
    String title;
    @ApiModelProperty(
            name = "内容"
    )
    String content;
}
